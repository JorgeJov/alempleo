import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AngularFireModule, FirebaseApp } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { firebaseConfig } from './../environments/environment'
import { ImageUploadModule } from "angular2-image-upload";

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ApplicantsGuard } from './guards/applicants/applicants.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth/auth.service';
import { ProfilePopoverComponent } from './components/profile-popover/profile-popover.component';
import { ApplicantComponent } from './components/applicant/applicant.component';
import { EnterprisesGuard } from './guards/enterprises/enterprises.guard';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { JobsComponent } from './components/jobs/jobs.component';
import { LoginOptionsComponent } from './components/login-options/login-options.component';

@NgModule({
    declarations: [
        AppComponent,
        ProfilePopoverComponent,
        ApplicantComponent,
        JobsComponent,
        LoginOptionsComponent
    ],
    entryComponents: [
        ProfilePopoverComponent,
        ApplicantComponent,
        JobsComponent,
        LoginOptionsComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot({
            mode: 'ios',
            backButtonText: 'Regresar',
        }),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        ImageUploadModule.forRoot(),
        HttpModule,

    ],
    providers: [
        StatusBar,
        SplashScreen,
        ApplicantsGuard,
        EnterprisesGuard,
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
            // provide: RouteReuseStrategy,
            // useClass: IonicRouteStrategy
        },
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
