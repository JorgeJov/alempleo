import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicantsGuard } from './guards/applicants/applicants.guard';
import { EnterprisesGuard } from './guards/enterprises/enterprises.guard';

const routes: Routes = [
  // { path: '**', redirectTo: 'landing'},
  { path: '', loadChildren: './pages/people/login/login.module#LoginPageModule'},
  { path: 'login/personas', loadChildren: './pages/people/login/login.module#LoginPageModule', canActivate: [ApplicantsGuard] },
  { path: 'registro/personas', loadChildren: './pages/people/signup/signup.module#SignupPageModule', canActivate: [ApplicantsGuard] },
  { path: 'dashboard/personas', loadChildren: './pages/people/dashboard/dashboard.module#DashboardPageModule', canActivate: [ApplicantsGuard] },
  { path: 'cuestionario/personas', loadChildren: './pages/people/starter/starter.module#StarterPageModule', canActivate: [ApplicantsGuard] },
  { path: 'plazas/categorias/:category', loadChildren: './pages/people/category/category.module#CategoryPageModule', canActivate: [ApplicantsGuard] },
  { path: 'configuraciones', loadChildren: './pages/people/configurations/configurations.module#ConfigurationsPageModule', canActivate: [ApplicantsGuard] },
  { path: 'plazas', loadChildren: './pages/people/jobs/jobs.module#JobsPageModule', canActivate: [ApplicantsGuard] },
  { path: 'plaza/:key', loadChildren: './pages/people/job/job.module#JobPageModule', canActivate: [ApplicantsGuard] },
  { path: 'perfil/personas', loadChildren: './pages/people/profile/profile.module#ProfilePageModule', canActivate: [ApplicantsGuard] },
  { path: 'notificaciones', loadChildren: './pages/people/notifications/notifications.module#NotificationsPageModule', canActivate: [ApplicantsGuard] },
  { path: 'buscar/:query', loadChildren: './pages/people/search/search.module#SearchPageModule', canActivate: [ApplicantsGuard] },

  //Empresas
  { path: 'cuestionario/empresas', loadChildren: './pages/enterprises/starter/starter.module#StarterPageModule', canActivate: [EnterprisesGuard] },
  { path: 'login/empresas', loadChildren: './pages/enterprises/login/login.module#LoginPageModule', canActivate: [EnterprisesGuard] },
  { path: 'registro/empresas', loadChildren: './pages/enterprises/signup/signup.module#SignupPageModule', canActivate: [EnterprisesGuard] },
  { path: 'dashboard/empresas', loadChildren: './pages/enterprises/dashboard/dashboard.module#DashboardPageModule', canActivate: [EnterprisesGuard] },
  { path: 'empresas/agregar_empleo', loadChildren: './pages/enterprises/add-job/add-job.module#AddJobPageModule', canActivate: [EnterprisesGuard] },
  { path: 'mis_plazas/:key', loadChildren: './pages/enterprises/job/job.module#JobPageModule', canActivate: [EnterprisesGuard] },
  { path: 'mis_plazas', loadChildren: './pages/enterprises/jobs/jobs.module#JobsPageModule', canActivate: [EnterprisesGuard] },
  { path: 'mis_plazas/:key/favs', loadChildren: './pages/enterprises/favs/favs.module#FavsPageModule', canActivate: [EnterprisesGuard] },
  { path: 'historial', loadChildren: './pages/enterprises/history/history.module#HistoryPageModule', canActivate: [EnterprisesGuard] },
  { path: 'configuraciones/:name', loadChildren: './pages/enterprises/configuraciones/configuraciones.module#ConfiguracionesPageModule', canActivate: [EnterprisesGuard] },
  { path: 'reportes', loadChildren: './pages/enterprises/reports/reports.module#ReportsPageModule', canActivate: [EnterprisesGuard] },
  { path: 'notificaciones/:name', loadChildren: './pages/enterprises/notifications/notifications.module#NotificationsPageModule', canActivate: [EnterprisesGuard] },
  { path: 'personas', loadChildren: './pages/enterprises/people/people.module#PeoplePageModule', canActivate: [EnterprisesGuard] },
  { path: 'persona/:name/:key', loadChildren: './pages/enterprises/person/person.module#PersonPageModule', canActivate: [EnterprisesGuard] },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
