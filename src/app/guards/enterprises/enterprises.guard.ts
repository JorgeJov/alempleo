import { Injectable } from '@angular/core'
import { Router, CanActivate } from '@angular/router'
import { AuthService } from './../../services/auth/auth.service'
import { ApplicantsService } from 'src/app/services/applicants/applicants.service';
import { testUserAgent } from '@ionic/core';
import { ReturnStatement } from '@angular/compiler';

@Injectable()
export class EnterprisesGuard implements CanActivate {

    constructor(
        private router: Router,
        private auth: AuthService,
        public applicantsService: ApplicantsService
    ) {

    }

    canActivate(route) {
        console.log(route.routeConfig.path);
        // Configurando los permisos para accesar como aplicante

        // Si entra a las paginas de login o de signup o login
        if (route.routeConfig.path == 'login/empresas' || route.routeConfig.path == 'registro/empresas') {
            // Si ya esta logueado entrando a las paginas de Signup o login
            if (this.auth.token) {
                //Verifica si es aplicante
                this.router.navigate(['/dashboard/empresas']);
                return true;
            } else {
                // Si No esta logueado Lo deja entrar a estas paginas
                return true;
            }
        } else {
            if (this.auth.token) {
                //Si esta logueado pues QUE ENTRE
                return true;
            } else {
                // Si No esta logueado al entrar a una pagina privada de aplicantes
                this.router.navigate(['/login/empresas'])
                return false
            }
        }
    }
}
