import { TestBed, async, inject } from '@angular/core/testing';

import { EnterprisesGuard } from './enterprises.guard';

describe('EnterprisesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnterprisesGuard]
    });
  });

  it('should ...', inject([EnterprisesGuard], (guard: EnterprisesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
