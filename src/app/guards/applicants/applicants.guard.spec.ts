import { TestBed, async, inject } from '@angular/core/testing';

import { ApplicantsGuard } from './applicants.guard';

describe('ApplicantsGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicantsGuard]
    });
  });

  it('should ...', inject([ApplicantsGuard], (guard: ApplicantsGuard) => {
    expect(guard).toBeTruthy();
  }));
});
