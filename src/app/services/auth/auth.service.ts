import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
const identifier = "token";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        public database: AngularFireDatabase,
        private angularAuth: AngularFireAuth,
        private router: Router,
        public http: Http
    ) {
        this.setUp();
    }
    token: string = this.getTokenFromLocalStorage();

    setUp() {
        this.token = this.getTokenFromLocalStorage();
        this.angularAuth.authState.subscribe((firebaseUser) => {
            if (firebaseUser) {
                localStorage.setItem(identifier, firebaseUser.uid);
                this.token = firebaseUser.uid;
            } else {
                localStorage.removeItem(identifier)
                this.token = null;
            }
        })
    }

    getTokenFromLocalStorage(): string {
        return localStorage.getItem(identifier)
    }

    login(email, password) {
        return new Promise((resolve, reject) => {
            this.angularAuth.auth.signInWithEmailAndPassword(email, password).then((data) => {
                resolve(data);
            }, (err) => {
                reject(err);
            })
        });
    }

    logOut() {
        return this.angularAuth.auth.signOut().then(() => {
            this.token = null;
            this.router.navigate(['/']);
        });
    }

    resetPassword(emailAddress) {
        let auth = firebase.auth();
        return new Promise((resolve, reject) => {
            auth.sendPasswordResetEmail(emailAddress).then(function () {
                resolve(true);
            }).catch(function (error) {
                reject(error);
            });
        });
    }

    getCurrentUser() {
        return firebase.auth().currentUser;
    }

    getApplicant(token) {
        return firebase.database().ref(`applicants/${token}/`);
    }

    getEnterpriseUser(token) {
        return firebase.database().ref(`user-enterprises/${token}/`);
    }

    signUpApplicant(data) {
        let self = this;
        return new Promise((resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(data.email, data.password).then(function (user_db) {
                user_db.user.updateProfile({
                    displayName: data.name,
                    photoURL: ''
                }).then(function () {

                    let user_key = user_db.user.uid;
                    let updates = {};

                    let account_data = {
                        email: data.email,
                        name: data.name,
                        lastname: data.lastname,
                        status: 'starter',
                        type: 'applicant',
                        alempleo: false,
                        created_at: firebase.database.ServerValue.TIMESTAMP,
                        last_login: firebase.database.ServerValue.TIMESTAMP,
                    }
                    updates[`/applicants/${user_key}`] = account_data;

                    firebase.database().ref().update(updates).then(data => {

                        let body = new FormData();
                        body.append('name', account_data.name);
                        body.append('email', account_data.email);
                        self.http.get('http://www.alempleo.org/mail/mail_confirm.php?email=' + account_data.email + '&name=' + account_data.name)
                            .subscribe(data => {
                                resolve(data);
                            });
                    }, (err) => {
                        reject(err);
                    });

                }, function (error) {
                    reject(error);
                });
            }, function (error) {
                reject(error);
                console.error(error);
            });
        });
    }


    signUpEnterprise(data) {
        let self = this;
        return new Promise((resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(data.email, data.password).then(function (user_db) {
                user_db.user.updateProfile({
                    displayName: data.name,
                    photoURL: ''
                }).then(function () {

                    let enterprise_key = firebase.database().ref().child('users').push().key;
                    let user_key = user_db.user.uid;
                    let updates = {};
                    let enterprise = {
                        key: enterprise_key,
                        name: data.enterprise
                    }

                    let account_data = {
                        email: data.email,
                        name: data.name,
                        lastname: data.lastname,
                        status: 'starter',
                        type: 'enterprise',
                        alempleo: false,
                        created_at: firebase.database.ServerValue.TIMESTAMP,
                        last_login: firebase.database.ServerValue.TIMESTAMP,
                        enterprise: enterprise
                    }

                    updates[`/enterprises/${enterprise_key}`] = { name: data.enterprise, status: 'starter', alempleo: false };
                    updates[`/user-enterprises/${user_key}`] = account_data;

                    firebase.database().ref().update(updates).then(data => {

                        let body = new FormData();
                        body.append('name', account_data.name);
                        body.append('email', account_data.email);
                        self.http.get('http://www.alempleo.org/mail/mail_confirm.php?email=' + account_data.email + '&name=' + account_data.name)
                            .subscribe(data => {
                                resolve(data);
                            });


                    }, (err) => {
                        reject(err);
                    });

                }, function (error) {
                    reject(error);
                });
            }, function (error) {
                reject(error);
                console.error(error);
            });
        });
    }
}
