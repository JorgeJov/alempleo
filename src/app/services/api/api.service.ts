import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { AuthService } from '../auth/auth.service';
import 'firebase/storage';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(
        private angularAuth: AngularFireAuth,
        public database: AngularFireDatabase,
        private auth: AuthService
    ) { }

    getWorkItems() {
        return firebase.database().ref(`work-items/`);
    }

    getEnterprise(key) {
        return firebase.database().ref(`enterprises/${key}`);
    }

    getEnterprises() {
        return firebase.database().ref(`enterprises/`);
    }

    getJobs() {
        return firebase.database().ref(`jobs/`);
    }

    getJobsFromEnterprise(enterprise_key) {
        return firebase.database().ref(`enterprise-jobs/${enterprise_key}/`);
    }

    getJob(key) {
        return firebase.database().ref(`jobs/${key}`);
    }

    getWorkItemJobs(kind) {
        return firebase.database().ref(`jobs-kinds/${kind}`);
    }

    getApplications() {
        return firebase.database().ref(`applications/`);
    }

    applyToAJob(job, applicant) {
        return new Promise((resolve, reject) => {
            let updates = {};
            applicant.applicant_key = applicant.key;

            let application_key = firebase.database().ref().child('applications').push().key;
            let notification_key = firebase.database().ref().child('notifications').push().key;

            let data = {
                applicant: applicant,
                job: job,
                date: this.GetTodayIsoFormat(new Date())
            };

            let notification_data = {
                to: job.enterprise,
                date: this.GetTodayIsoFormat(new Date()),
                from: applicant,
                reason: 'applyToAJob',
                job: job,
                new: true
            }

            updates[`/applications-jobs/${job.key}/${application_key}`] = applicant;
            updates[`/applications/${application_key}`] = data;
            updates[`/notifications/${job.enterprise.key}/${notification_key}`] = notification_data;

            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    markReadANotification(notification) {
        return new Promise((resolve, reject) => {
            let updates = {};
            updates[`/notifications/${notification.job.enterprise.key}/${notification.key}/new`] = false;

            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    markReadANotificationPerson(notification) {
        return new Promise((resolve, reject) => {
            let updates = {};
            updates[`/notifications/${notification.applicant.key}/${notification.key}/new`] = false;

            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    GetJobApplicant(job_key) {
        return firebase.database().ref(`applications-jobs/${job_key}/`);
    }

    GetJobApplicants(job_key) {
        return firebase.database().ref(`applications-jobs/${job_key}/`);
    }

    GetApplicant(key) {
        return firebase.database().ref(`applicants/${key}`);
    }

    GetApplicantions() {
        return firebase.database().ref(`applications/`);
    }

    getApplicant(key) {
        return firebase.database().ref(`applicants/${key}`);
    }

    getNotifications(account_key) {
        return firebase.database().ref(`notifications/${account_key}`);
    }

    getPeople() {
        return firebase.database().ref(`applicants/`);
    }

    IsApplicant() {
        return new Promise((resolve, reject) => {
            // firebase.database().ref(`applicants/${this.auth.token}/`)
            //     .on('value', Snapshot => {
            //         let user = Snapshot.val();
            //         if (user) {
            //             resolve(true);
            //         } else {
            //             resolve(false);
            //         }
            //     })
        });
    }

    starterInfo(starter_data, CVFile) {
        let self = this;

        return new Promise((resolve, reject) => {

            if (starter_data.image) {
                this.uploadImage(this.auth.token, starter_data.image).then(imageData => {
                    starter_data.image = imageData;
                    self.addCV(starter_data, resolve, reject, CVFile);
                });
            } else {
                self.addCV(starter_data, resolve, reject, CVFile);
            }
        });
    }

    addCV(starter_data, resolve, reject, CVFile) {
        let self = this;
        if (CVFile) {
            let storageRef = firebase.storage().ref();
            const filename = this.generateUUID();
            let fileRef = storageRef.child(`cv/${this.auth.token}/${filename}`);
            fileRef.put(CVFile).then(data => {
                fileRef.getDownloadURL().then(function (downloadURL) {
                    starter_data.cv = downloadURL;
                    self.afterStart(starter_data, resolve, reject)
                });
            });
        } else {
            self.afterStart(starter_data, resolve, reject)
        }
    }


    afterStart(starter_data, resolve, reject) {
        let updates = {};
        starter_data.status = 'pending_authorization';
        updates[`/applicants/${this.auth.token}`] = starter_data;
        updates[`/images/${this.auth.token}/`] = starter_data.image;
        firebase.database().ref().update(updates).then(data => {
            resolve(data);
        }, (err) => {
            reject(err);
        });
    }

    starterInfoEnterprise(enterprise_key, first, second) {
        return new Promise((resolve, reject) => {
            this.uploadImage(this.auth.token, first.image).then(imageData => {
                first.image = imageData;
                let updates = {};
                updates[`/enterprises/${enterprise_key}/email`] = first.email_contact;
                updates[`/enterprises/${enterprise_key}/phone`] = first.phone_contact;
                updates[`/enterprises/${enterprise_key}/branches/`] = second.branches;
                updates[`/enterprises/${enterprise_key}/status`] = 'pending_authorization';
                updates[`/images/${enterprise_key}/`] = first.image;

                firebase.database().ref().update(updates).then(data => {
                    resolve(data);
                }, (err) => {
                    reject(err);
                });

            });
        });
    }

    addJob(data, user) {
        let job_key = firebase.database().ref().child('jobs').push().key;
        data.date = this.GetTodayIsoFormat(new Date());
        return new Promise((resolve, reject) => {
            let updates = {};
            data.opened_by = user;
            data.status = 'abierta';
            updates[`/jobs/${job_key}`] = data;
            updates[`/enterprise-jobs/${user.enterprise.key}/${job_key}`] = data;
            updates[`/kind-jobs/${data.kind_job.key}/${job_key}`] = data;
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    addWorkItem(data) {


        return new Promise((resolve, reject) => {
            let work_item_key = firebase.database().ref().child('work-items').push().key;
            let updates = {};
            updates[`/work-items/${work_item_key}`] = data;
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    addToFavs(job, applicant) {
        return new Promise((resolve, reject) => {
            let updates = {};
            let fav_key = firebase.database().ref().child(`favorite-applicants/${job.key}`).push().key;
            let data = {
                job: job,
                applicant: applicant
            };
            updates[`favorite-applicants/${job.key}/${fav_key}`] = data;
            updates[`favorites_history/${fav_key}`] = data;
            updates[`applications/${applicant.key}/status_application/`] = 'fav';
            updates[`applications-jobs/${job.key}/${applicant.key}/status_application`] = 'fav';
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    removeToFavs(job_key, fav_key) {
        return new Promise((resolve, reject) => {
            let updates = {};

            updates[`favorite-applicants/${job_key}/${fav_key}`] = null;
            updates[`favorites_history/${fav_key}`] = null;
            updates[`applications/${fav_key}/status_application/`] = '';
            updates[`applications-jobs/${job_key}/${fav_key}/status_application`] = '';
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    getFavs(job_key) {
        return firebase.database().ref(`favorite-applicants/${job_key}/`);
    }

    getPicture(key) {
        return firebase.database().ref(`/images/${key}`);
    }

    addContratation(contratation) {
        return new Promise((resolve, reject) => {
            let contratation_key = firebase.database().ref().child(`contratations`).push().key;
            let notification_key = firebase.database().ref().child(`contratations`).push().key;
            let updates = {};

            let notification_data = {
                from: contratation.job.enterprise,
                date: this.GetTodayIsoFormat(new Date()),
                to: contratation.applicant,
                reason: 'hired',
                job: contratation.job,
                new: true
            }

            contratation.date = this.GetTodayIsoFormat(new Date());
            updates[`contratations/${contratation_key}`] = contratation;
            updates[`jobs/${contratation.job.key}/status/`] = 'cerrada';
            updates[`applications-jobs/${contratation.job.key}/${contratation.applicant.key}/status_application/`] = 'hired';
            updates[`enterprise-jobs/${contratation.job.enterprise.key}/${contratation.job.key}/status/`] = 'cerrada';
            updates[`kind-jobs/${contratation.job.kind_job.key}/${contratation.job.key}/status/`] = 'cerrada';
            updates[`/notifications/${contratation.applicant.key}/${notification_key}`] = notification_data;
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    getContratation() {
        return firebase.database().ref(`contratations/`);
    }

    UpdateApplicant(key, data) {
        return new Promise((resolve, reject) => {


            this.database.database.ref(`/applicants/${key}/`).update(data).then(data => {
                resolve(data);
            }, err => {
                reject(err);
            })
        });
    }


    UpdateEnterprise(key, data) {
        return new Promise((resolve, reject) => {
            this.database.database.ref(`/enterprises/${key}/`).update(data).then(data => {
                resolve(data);
            }, err => {
                reject(err);
            })
        });
    }

    GetTodayIsoFormat(date) {
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let dt = date.getDate();
        let dt2, month2;

        if (dt < 10) {
            dt2 = '0' + dt;
        } else {
            dt2 = dt;
        }
        if (month < 10) {
            month2 = '0' + month;
        } else {
            month2 = month;
        }

        return `${year}-${month2}-${dt2}`;
    }



    uploadImage(user_key, image) {
        return new Promise((resolve, reject) => {

            let storageRef = firebase.storage().ref();
            const filename = this.generateUUID();
            let imageRef = storageRef.child(`images/${filename}.jpg`);

            imageRef.putString(image, firebase.storage.StringFormat.DATA_URL)
                .then(data => {
                    let photoData = {};
                    photoData['fullPath'] = data.metadata.fullPath;
                    photoData['size'] = data.metadata.size;
                    photoData['contentType'] = data.metadata.contentType;
                    photoData['md5Hash'] = data.metadata.md5Hash;
                    photoData['bucket'] = data.metadata.bucket;
                    photoData['updated'] = data.metadata.updated;
                    imageRef.getDownloadURL().then(data => {
                        photoData['downloadURL'] = data;
                        resolve(photoData);
                    });
                })
        })
    }

    private generateUUID(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }



    updatePersonProfile(key, data, cv) {
        let self = this;

        return new Promise((resolve, reject) => {
            if (data.image) {
                this.uploadImage(key, data.image).then(imageData => {
                    this.database.database.ref(`/images/${key}/`).update(imageData).then(rsesult => {
                        self.updateCV(data, resolve, reject, cv, key);
                    }, err => {
                        reject(err);
                    })
                });
            } else {
                self.updateCV(data, resolve, reject, cv, key);
            }
        });
    }

    updateCV(starter_data, resolve, reject, CVFile, user_key) {
        let self = this;
        console.log(CVFile);

        if (CVFile) {
            let storageRef = firebase.storage().ref();
            const filename = this.generateUUID();
            let fileRef = storageRef.child(`cv/${user_key}/${filename}`);
            fileRef.put(CVFile).then(data => {
                fileRef.getDownloadURL().then(function (downloadURL) {
                    starter_data.cv = downloadURL;
                    self.updateFinal(starter_data, resolve, reject, user_key)
                });
            });
        } else {
            delete starter_data.cv;
            self.updateFinal(starter_data, resolve, reject, user_key)
        }
    }

    updateFinal(starter_data, resolve, reject, user_key) {

        this.database.database.ref(`/applicants/${user_key}/`).update(starter_data).then(data => {
            resolve(data);
        }, err => {
            reject(err);
        })
    }

    updateEnterprise(data, enterprise_key) {

        return new Promise((resolve, reject) => {
            let enterprise = {
                name: data.enterprise,
                branches: data.branches,
                email_contact: data.email_contact,
                phone_contact: data.phone_contact,
                name_contact: data.phone_contact,
                lastname_contact: data.phone_contact,
            }

            let updates = {};


            if (data.image) {
                this.uploadImage(enterprise_key, data.image).then(imageData => {
                    this.database.database.ref(`/images/${enterprise_key}/`).update(imageData).then(rsesult => {
                        this.database.database.ref(`/enterprises/${enterprise_key}/`).update(enterprise).then(data => {
                            resolve(data);
                        }, err => {
                            reject(err);
                        })
                    }, err => {
                        reject(err);
                    })
                });
            } else {
                this.database.database.ref(`/enterprises/${enterprise_key}/`).update(enterprise).then(data => {
                    resolve(data);
                }, err => {
                    reject(err);
                })
            }
        })
    }
}

