import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { AuthService } from '../auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class ApplicantsService {

    constructor(
        private angularAuth: AngularFireAuth,
        public database: AngularFireDatabase,
        private auth: AuthService
    ) {
        console.log('Hello Applicants Service');
    }

    getApplicant() {
        return firebase.database().ref(`applicants/${this.auth.token}/`);
    }

    IsApplicant() {
        return new Promise((resolve, reject) => {
            // firebase.database().ref(`applicants/${this.auth.token}/`)
            //     .on('value', Snapshot => {
            //         let user = Snapshot.val();
            //         if (user) {
            //             resolve(true);
            //         } else {
            //             resolve(false);
            //         }
            //     })
        });
    }
}
