import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { ApplicantComponent } from 'src/app/components/applicant/applicant.component';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-job',
    templateUrl: './job.page.html',
    styleUrls: ['./job.page.scss'],
})
export class JobPage implements OnInit {

    job_key: string;
    enterprise: any;
    applicantsData: boolean = false;
    jobs: any = [];
    job: any;
    applicants: any;
    user: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private auth: AuthService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        public modalController: ModalController
    ) {
        this.job_key = this.route.snapshot.paramMap.get('key');

        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getEnterpriseUser(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getEnterprise(this.user.enterprise.key)
                                .on('value', Snapshot => {
                                    this.enterprise = Snapshot.val();
                                    this.enterprise.key = Snapshot.key;
                                    this.api.getPicture(this.enterprise.key)
                                        .on('value', Snapshot => {
                                            this.enterprise.image = Snapshot.val();
                                            
                                            this.api.getNotifications(this.enterprise.key)
                                                .orderByChild('new')
                                                .equalTo(true)
                                                .on('value', snapshots => {
                                                    this.enterprise.notifications = snapshots.numChildren();
                                                    this.getAllData();
                                                    loader.dismiss();
                                                });

                                        });
                                });
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }


    getAllData() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.getJob(this.job_key)
                    .on('value', Snapshot => {
                        let job = Snapshot.val();
                        job.key = Snapshot.key;
                        this.job = job;
                        loader.dismiss();
                    });
            });
        });

        this.api.GetJobApplicants(this.job_key)
            .on('value', result => {
                this.applicants = [];
                result.forEach(element => {
                    let applicant = element.val();
                    applicant.key = element.key;
                    this.api.getPicture(applicant.applicant_key)
                        .on('value', Snapshot => {
                            applicant.image = Snapshot.val();
                            this.applicants.push(applicant);
                        })
                });
                console.log(this.applicants);

                this.applicantsData = true;
                this.forceUpdate();
            })
    }
    ngOnInit() {
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    async previewApplicant(applicant) {

        const modal = await this.modalController.create({
            component: ApplicantComponent,
            componentProps: {
                applicant: applicant,
                job: this.job
            }
        });
        return await modal.present();
    }

    addTopFavs(applicant) {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.addToFavs(this.job, applicant).then(data => {
                    loader.dismiss();
                });
            });
        });
    }

}
