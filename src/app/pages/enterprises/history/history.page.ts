import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ApplicantComponent } from 'src/app/components/applicant/applicant.component';

@Component({
    selector: 'app-history',
    templateUrl: './history.page.html',
    styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

    jobsData: boolean = false;
    jobs: any = [];
    user: any;
    enterprise: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        private auth: AuthService,
        public modalController: ModalController
    ) {

        this.auth.getEnterpriseUser(this.auth.token)
            .on('value', Snapshot => {
                this.user = Snapshot.val();
                if (this.user) {
                    this.user.key = Snapshot.key;
                    this.api.getEnterprise(this.user.enterprise.key)
                        .on('value', Snapshot => {
                            this.enterprise = Snapshot.val();
                            this.enterprise.key = Snapshot.key;
                            this.api.getPicture(this.enterprise.key)
                                .on('value', Snapshot => {
                                    this.enterprise.image = Snapshot.val();
                                    this.api.getNotifications(this.enterprise.key)
                                        .orderByChild('new')
                                        .equalTo(true)
                                        .on('value', snapshots => {
                                            this.enterprise.notifications = snapshots.numChildren();
                                            this.getAllData();

                                        });
                                });
                        });
                } else {
                    this.auth.logOut();
                }
            });
    }

    ngOnInit() {
    }

    getAllData() {
        this.api.getJobsFromEnterprise(this.enterprise.key)
            .orderByChild('status')
            .equalTo('cerrada')
            .on('value', snapshot => {

                snapshot.forEach(childSnapshot => {
                    let item = childSnapshot.val();
                    item.key = childSnapshot.key;
                    this.api.getContratation()
                        .orderByChild('job/key')
                        .equalTo(item.key)
                        .on('value', Snapshots => {
                            Snapshots.forEach(element => {
                                let contratation = element.val();
                                contratation.key = element.key;
                                item.contratation = contratation;
                            });

                            this.jobs.push(item);
                            this.jobsData = true;
                            this.forceUpdate();
                        })
                });

                this.jobsData = true;
                this.forceUpdate();
            });

    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    async previewApplicant(applicant) {

        const modal = await this.modalController.create({
            component: ApplicantComponent,
            componentProps: {
                applicant: applicant
            }
        });
        return await modal.present();
    }

}
