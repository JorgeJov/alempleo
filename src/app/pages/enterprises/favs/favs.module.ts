import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FavsPage } from './favs.page';
import { SharedEnterpriseComponentsModule } from 'src/app/components/shared-enterprise-components.module';

const routes: Routes = [
  {
    path: '',
    component: FavsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedEnterpriseComponentsModule
  ],
  declarations: [FavsPage]
})
export class FavsPageModule {}
