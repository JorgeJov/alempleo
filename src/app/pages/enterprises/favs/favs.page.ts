import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { ApplicantComponent } from 'src/app/components/applicant/applicant.component';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-favs',
    templateUrl: './favs.page.html',
    styleUrls: ['./favs.page.scss'],
})
export class FavsPage implements OnInit {

    job_key: string;
    enterprise: any;
    favoritesData: boolean = false;
    jobs: any = [];
    job: any;
    favorites: any;
    user: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        public modalController: ModalController,
        private auth: AuthService,
    ) {
        this.job_key = this.route.snapshot.paramMap.get('key');

        this.loader().then(loader => {

            this.auth.getEnterpriseUser(this.auth.token)
                .on('value', Snapshot => {
                    this.user = Snapshot.val();
                    if (this.user) {
                        this.user.key = Snapshot.key;
                        this.api.getEnterprise(this.user.enterprise.key)
                            .on('value', Snapshot => {
                                this.enterprise = Snapshot.val();
                                this.enterprise.key = Snapshot.key;
                                this.api.getPicture(this.enterprise.key)
                                    .on('value', Snapshot => {
                                        this.enterprise.image = Snapshot.val();
                                        this.api.getNotifications(this.enterprise.key)
                                            .orderByChild('new')
                                            .equalTo(true)
                                            .on('value', snapshots => {
                                                this.enterprise.notifications = snapshots.numChildren();
                                                this.getAllData();
                                                loader.dismiss();
                                            });
                                    });
                            });
                    } else {
                        loader.dismiss();
                        this.auth.logOut();
                    }
                });


        });
    }

    ngOnInit() {
    }

    getAllData() {


        this.api.getJob(this.job_key)
            .on('value', Snapshot => {
                let job = Snapshot.val();
                job.key = Snapshot.key;
                this.job = job;
            });


        this.api.getFavs(this.job_key)
            .on('value', result => {
                this.favorites = [];
                result.forEach(element => {
                    let applicant = element.val();
                    applicant.fav_key = element.key;
                    this.api.getPicture(applicant.applicant_key)
                        .on('value', Snapshot => {
                            this.favorites.push(applicant);
                            this.forceUpdate();
                        })
                    this.favoritesData = true;
                    this.forceUpdate();
                });
            })

    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    async previewApplicant(applicant) {
        console.log(applicant);
        const modal = await this.modalController.create({
            component: ApplicantComponent,
            componentProps: { applicant: applicant }
        });
        return await modal.present();
    }

    async contratation(fav) {
        if (this.job.status == 'abierta') {

            const alert = await this.alertController.create({
                header: ``,
                message: `Confirma que deseas contratar a <strong>${fav.applicant.name}</strong> para la plaza de <strong>${this.job.position}</strong>`,
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'cancelButtonAlert',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Contratar',
                        cssClass: 'confirmButtonAlert',
                        handler: () => {
                            this.loader().then(loader => {
                                loader.present().then(() => {

                                    this.api.addContratation(fav).then(data => {
                                        this.job.status = 'cerrada';
                                        loader.dismiss();
                                    });

                                });
                            });
                        }
                    }
                ]
            });

            await alert.present();
        } else {
            this.presentAlert();
        }
    }

    async presentAlert() {
        const alert = await this.alertController.create({
            header: 'Ups!',
            message: 'Esta plaza ya se encuentra cerrada',
            buttons: ['OK']
        });

        await alert.present();
    }

    async removeFromFavs(fav) {

        const alert = await this.alertController.create({
            header: ``,
            message: `Solo confirma que deseas quitar a <strong>${fav.applicant.name}</strong> para la plaza de <strong>${this.job.position}</strong>?`,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'cancelButtonAlert',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Quitar de favoritos',
                    cssClass: 'confirmButtonAlert',
                    handler: () => {
                        this.loader().then(loader => {
                            loader.present().then(() => {

                                this.api.removeToFavs(this.job.key, fav.fav_key).then(data => {
                                    loader.dismiss();
                                });

                            });
                        });
                    }
                }
            ]
        });

        await alert.present();


    }

}
