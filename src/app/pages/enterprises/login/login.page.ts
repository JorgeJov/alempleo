import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoginOptionsComponent } from 'src/app/components/login-options/login-options.component';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    loginForm: FormGroup;

    constructor(
        public formBuilder: FormBuilder,
        private auth: AuthService,
        public alertCtrl: AlertController,
        private router: Router,
        public loadingCtrl: LoadingController,
        public popoverController: PopoverController
    ) {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]]
        });
    }

    ngOnInit() {
        console.log('Login Page');
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    login() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.login(this.loginForm.value.email, this.loginForm.value.password).then(data => {
                    loader.dismiss();
                    this.router.navigate(['/dashboard/empresas'], )
                }, err => {
                    this.alertCtrl.create({
                        header: '¡Ups!',
                        // subHeader: 'Subtitle',
                        message: 'Hay un error con tus correo o contraseña, intentalo nuevamente',
                        buttons: ['Aceptar']
                    }).then(alert => {
                        loader.dismiss();
                        alert.present();
                    })
                });
            });
        });
    }

    async forgotPassword() {
        const alert = await this.alertCtrl.create({
            header: 'Ingresa tu dirección de correo electronico',
            inputs: [
                {
                    name: 'email',
                    type: 'text',
                    placeholder: 'Correo electronico'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        this.auth.resetPassword(data.email).then(data=>{
                            this.alertCtrl.create({
                                header: 'Restaurar contraseña',
                                message: 'Hemos enviado un link a tu correo electornico para que puedas restaurar tu contraseña',
                                buttons: ['Aceptar']
                            }).then(alert => {
                                alert.present();
                            })
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    async presentPopover(ev: any) {
        const popover = await this.popoverController.create({
            component: LoginOptionsComponent,
            event: ev,
            translucent: true
        });
        return await popover.present();
    }



}

