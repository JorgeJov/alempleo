import { Component, OnInit, NgZone } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { ProfilePopoverComponent } from 'src/app/components/profile-popover/profile-popover.component';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ApiService } from 'src/app/services/api/api.service';
import { snapshotChanges } from 'angularfire2/database';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
    selector: 'app-add-job',
    templateUrl: './add-job.page.html',
    styleUrls: ['./add-job.page.scss'],
})
export class AddJobPage implements OnInit {

    user: any;
    enterprise: any;
    work_items: any;
    aptitudes: any = [];
    addWorkForm: FormGroup;

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverCtrl: PopoverController,
        public api: ApiService,
        public alertCtrl: AlertController,
        public formBuilder: FormBuilder,
        public alertController: AlertController,
        private zone: NgZone
    ) {
        this.addWorkForm = this.formBuilder.group({
            position: ['', [Validators.required]],
            salary: ['', [Validators.required]],
            branch: ['', [Validators.required]],
            type: ['', [Validators.required]],
            experience: ['', [Validators.required]],
            kind_job: ['', [Validators.required]],
            gender: ['', [Validators.required]],
            description: ['', [Validators.required]],
            number: ['', [Validators.required]],
            expiration: ['', [Validators.required]],
            aptitudes: [''],
        });

        this.api.getWorkItems()
            .on('value', Snapshots => {
                this.work_items = [];
                Snapshots.forEach(element => {
                    let work_item = element.val();
                    work_item.key = element.key;
                    this.work_items.push(work_item);
                });
            })

        this.loader().then(loader => {
            loader.present().then(() => {

                this.auth.getEnterpriseUser(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getEnterprise(this.user.enterprise.key)
                                .on('value', Snapshot => {
                                    this.enterprise = Snapshot.val();
                                    this.enterprise.key = Snapshot.key;
                                    this.api.getPicture(this.enterprise.key)
                                        .on('value', Snapshot => {
                                            this.enterprise.image = Snapshot.val();
                                            this.api.getNotifications(this.enterprise.key)
                                                .orderByChild('new')
                                                .equalTo(true)
                                                .on('value', snapshots => {
                                                    this.enterprise.notifications = snapshots.numChildren();
                                                    loader.dismiss();
                                                    this.forceUpdate();
                                                });
                                        });
                                });
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    async presentProfilePopover(ev: any) {
        const popover = await this.popoverCtrl.create({
            component: ProfilePopoverComponent,
            event: ev,
        });
        return await popover.present();
    }

    async addAptitud() {
        const alert = await this.alertCtrl.create({
            header: 'Agrega aptitud',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Aptitud'
                },

            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: data => {
                        this.aptitudes.push(data);
                        this.addWorkForm.controls['aptitudes'].setValue(this.aptitudes);
                    }
                }
            ]
        });

        await alert.present();
    }

    removeAptitud(aptitud, index) {
        this.aptitudes.splice(index, 1);
        if (this.isEmpty(this.aptitudes)) {
            this.addWorkForm.controls['aptitudes'].setValue('');
        } else {
            this.addWorkForm.controls['aptitudes'].setValue(this.aptitudes);
        }
    }

    isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }


    addWork() {
        this.loader().then(loader => {
            loader.present().then(() => {
                let data = this.addWorkForm.value;
                data.enterprise = this.enterprise;
                this.api.addJob(this.addWorkForm.value, this.user).then(data => {
                    loader.dismiss();
                    this.presentAlert('Plaza agregada exitosamente', 'Felicidades tu plaza ah sido agregada correctamente');
                }, err => {
                    loader.dismiss();
                });
            });
        });
    }

    async presentAlert(title, message) {
        const alert = await this.alertController.create({
            header: title,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
    }

}
