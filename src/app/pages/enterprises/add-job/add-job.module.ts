import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddJobPage } from './add-job.page';
import { SharedEnterpriseComponentsModule } from 'src/app/components/shared-enterprise-components.module';

const routes: Routes = [
  {
    path: '',
    component: AddJobPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedEnterpriseComponentsModule
  ],
  declarations: [AddJobPage]
})
export class AddJobPageModule {}
 