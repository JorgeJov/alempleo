import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { PopoverController, LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-people',
    templateUrl: './people.page.html',
    styleUrls: ['./people.page.scss'],
})
export class PeoplePage implements OnInit {

    user: any;
    enterprise: any;

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverCtrl: PopoverController,
        public api: ApiService,
        private zone: NgZone,
        public alertController: AlertController
    ) {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getEnterpriseUser(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;

                            this.api.getEnterprise(this.user.enterprise.key)
                                .on('value', Snapshot => {
                                    this.enterprise = Snapshot.val();
                                    this.enterprise.key = Snapshot.key;
                                    this.api.getPicture(this.enterprise.key)
                                        .on('value', Snapshot => {

                                            this.enterprise.image = Snapshot.val();
                                            this.api.getNotifications(this.enterprise.key)
                                                .orderByChild('new')
                                                .equalTo(true)
                                                .on('value', snapshots => {
                                                    this.enterprise.notifications = snapshots.numChildren();
                                                    this.getAllData();
                                                    loader.dismiss();
                                                });
                                        });
                                });
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {

    }

    peopleData;
    people: any;
    people_full: any;

    searching: any = false;
    searchTerm: string = '';

    work_items: any;

    getAllData() {
        this.api.getPeople()
            .on('value', Snapshots => {
                this.people = [];
                this.people_full = [];
                Snapshots.forEach(element => {
                    let item = element.val();
                    item.key = element.key;
                    item.name_url = item.name.replace(" ", "_");
                    item.name_url += '_' + item.lastname.replace(" ", "_");
                    this.api.getPicture(item.key)
                        .on('value', Snapshot => {
                            item.image = Snapshot.val();
                            if (item.image == null) {
                                item.image = {
                                    downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                }
                            }
                            if (item.birthdate !== undefined) {
                                item.birthdate = this.calculateAge(new Date(item.birthdate));
                            }

                            item.searchQ = `${item.name} ${item.lastname} ${item.email} ${item.salary} ${item.status} ${item.experience} ${item.aptitudes} ${item.birthdate} años`;
                            for (var prop in item.interests) {
                                item.searchQ += `${item.interests[prop]['name']} `;
                            }
                            this.people_full.push(item)
                            this.people.push(item)
                            this.forceUpdate();
                        })
                });
                this.peopleData = true;
                this.forceUpdate();
            });

        this.api.getWorkItems()
            .on('value', Snapshots => {
                this.work_items = [];
                Snapshots.forEach(element => {
                    let work_item = element.val();
                    work_item.key = element.key;

                    let data = {
                        type: 'checkbox',
                        label: work_item.name,
                        value: work_item.name
                    }

                    this.work_items.push(data);
                });
            });
    }

    calculateAge(birthday) {
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }



    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    setSearch(ev) {
        let searchTerm = '';
        if (ev !== null) {
            searchTerm = ev.target.value;
        }

        if (this.filter) {
            this.filter.forEach(element => {
                searchTerm += `${element}`
            });
        }


        if (searchTerm == '') {
            this.people = this.people_full;
        } else {
            console.log(searchTerm);

            this.people = this.people_full.filter((item) => {
                return item.searchQ.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
        }
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    filter: any;

    async presentAlertRadio() {
        const alert = await this.alertController.create({
            header: 'Radio',
            inputs: this.work_items,
            buttons: [
                {
                    text: 'Limpiar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        this.work_items.forEach(element => {
                            element.checked = false;
                        });
                        this.filter = false;
                        this.setSearch(null)
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        this.filter = data;

                        this.work_items.forEach(element => {
                            element.checked = false;
                        });

                        this.filter.forEach(element => {
                            let ele = this.work_items.find(item => item.label == element);
                            ele.checked = true;
                        });

                        this.setSearch(null)
                    }
                }
            ]
        });

        await alert.present();
    }

    containsObject(obj, list) {
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i].key == obj.key) {
                return true;
            }
        }

        return false;
    }

}
