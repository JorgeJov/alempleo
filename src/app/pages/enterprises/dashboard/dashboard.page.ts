import { Component, OnInit, NgZone } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { ProfilePopoverComponent } from 'src/app/components/profile-popover/profile-popover.component';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

    user: any;
    enterprise: any;
    jobsData: boolean = false;
    jobs: any = [];
    jobs_opens: any = [];

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverCtrl: PopoverController,
        public api: ApiService,
        private zone: NgZone
    ) {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getEnterpriseUser(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getEnterprise(this.user.enterprise.key)
                                .on('value', Snapshot => {
                                    this.enterprise = Snapshot.val();
                                    this.enterprise.key = Snapshot.key;
                                    this.api.getPicture(this.enterprise.key)
                                        .on('value', Snapshot => {

                                            this.enterprise.image = Snapshot.val();
                                            this.api.getNotifications(this.enterprise.key)
                                                .orderByChild('new')
                                                .equalTo(true)
                                                .on('value', snapshots => {
                                                    this.enterprise.notifications = snapshots.numChildren();
                                                    this.getAllData();
                                                    loader.dismiss();
                                                });
                                        });
                                });
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {
    }

    getAllData() {
        this.api.getJobsFromEnterprise(this.enterprise.key)
            .orderByChild('status')
            .equalTo('abierta')
            .limitToLast(5)
            .on('value', Snapshots => {
                this.jobs = [];
                Snapshots.forEach(Snapshot => {
                    let item = Snapshot.val();
                    item.key = Snapshot.key;
                    item.applicants = [];

                    this.api.GetJobApplicants(item.key)
                        .limitToLast(6)
                        .on('value', result => {

                            result.forEach(element => {
                                let applications = element.val()
                                applications.key = element.key;

                                this.api.GetApplicant(applications.applicant_key)
                                    .on('value', Snapshot => {
                                        let aplicante = Snapshot.val();
                                        aplicante.key = Snapshot.key;
                                        applications.applicant = aplicante;
                                        this.api.getPicture(aplicante.key)
                                            .on('value', Snapshot => {
                                                applications.applicant.image = Snapshot.val();
                                                item.applicants.push(applications);
                                                this.forceUpdate();
                                            });
                                    })
                            });
                            this.jobs.push(item)
                            this.jobsData = true;
                            this.forceUpdate();
                        })
                });
                this.forceUpdate();
                console.log(this.jobs);

            });

        this.api.getJobsFromEnterprise(this.enterprise.key)
            .orderByChild('status')
            .equalTo('abierta')
            .limitToLast(5)
            .on('value', Snapshots => {
                this.jobs_opens = [];
                Snapshots.forEach(Snapshot => {
                    let item = Snapshot.val();
                    item.key = Snapshot.key;
                    item.applicants = [];

                    this.api.GetJobApplicants(item.key)
                        .limitToLast(6)
                        .on('value', result => {

                            result.forEach(element => {
                                let applicant = element.val();
                                applicant.key = element.key;
                                this.api.getPicture(applicant.key)
                                    .on('value', Snapshot => {
                                        applicant.image = Snapshot.val();
                                        item.applicants.push(applicant);
                                        this.forceUpdate();
                                    });
                            });
                            this.jobs_opens.push(item)
                            this.jobsData = true;
                            this.forceUpdate();
                        })
                });
            });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

}
