import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { PopoverController, LoadingController, AlertController, ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { JobsComponent } from 'src/app/components/jobs/jobs.component';

@Component({
    selector: 'app-person',
    templateUrl: './person.page.html',
    styleUrls: ['./person.page.scss'],
})
export class PersonPage implements OnInit {

    user: any;
    enterprise: any;

    personName: string;
    person: any;
    jobsData: boolean = false;
    person_key: any;
    jobs: any = [];
    interests = [];

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverCtrl: PopoverController,
        public api: ApiService,
        private zone: NgZone,
        private route: ActivatedRoute,
        public alertController: AlertController,
        public modalController: ModalController
    ) {

        this.personName = this.route.snapshot.paramMap.get('name');
        this.personName = this.personName.replace("_", " ")
        this.person_key = this.route.snapshot.paramMap.get('key');

        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getEnterpriseUser(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getEnterprise(this.user.enterprise.key)
                                .on('value', Snapshot => {
                                    this.enterprise = Snapshot.val();
                                    this.enterprise.key = Snapshot.key;
                                    this.api.getPicture(this.enterprise.key)
                                        .on('value', Snapshot => {

                                            this.enterprise.image = Snapshot.val();
                                            this.api.getNotifications(this.enterprise.key)
                                                .orderByChild('new')
                                                .equalTo(true)
                                                .on('value', snapshots => {
                                                    this.enterprise.notifications = snapshots.numChildren();
                                                    this.getAllData();
                                                    loader.dismiss();
                                                });
                                        });
                                });
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {

    }

    async addToaJob() {
        const modal = await this.modalController.create({
            component: JobsComponent,
            componentProps: { applicant: this.person, enterprise: this.enterprise }
        });
        return await modal.present();
    }

    getAllData() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.getApplicant(this.person_key)
                    .orderByChild('name')
                    .on('value', resp => {
                        let person = resp.val();
                        person.key = resp.key;
                        if (person.birthdate) {
                            person.birthdate = this.calculateAge(new Date(person.birthdate));
                        }
                        for (var prop in person.interests) {
                            this.interests.push(person.interests[prop]);
                        }
                        this.api.getPicture(person.key)
                            .on('value', Snapshot => {

                                person.image = Snapshot.val();

                                if (person.image == null) {
                                    person.image = {
                                        downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                    }
                                }
                                this.person = person;
                                console.log(person);

                                loader.dismiss();
                                this.forceUpdate();
                            })
                    });
            });
        });
    }

    calculateAge(birthday) {
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }


    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

}
