import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkplacesPage } from './workplaces.page';

describe('WorkplacesPage', () => {
  let component: WorkplacesPage;
  let fixture: ComponentFixture<WorkplacesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkplacesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkplacesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
