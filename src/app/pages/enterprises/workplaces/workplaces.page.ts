import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-workplaces',
    templateUrl: './workplaces.page.html',
    styleUrls: ['./workplaces.page.scss'],
})
export class WorkplacesPage implements OnInit {

    jobsData: boolean = false;
    jobs: any = [];
    user: any;
    enterprise: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        private auth: AuthService
    ) {

        this.auth.getEnterpriseUser(this.auth.token)
            .on('value', Snapshot => {
                this.user = Snapshot.val();
                if (this.user) {
                    this.user.key = Snapshot.key;
                    this.api.getEnterprise(this.user.enterprise.key)
                        .on('value', Snapshot => {
                            this.enterprise = Snapshot.val();
                            this.enterprise.key = Snapshot.key;
                            this.getAllData();
                        });
                } else {
                    this.auth.logOut();
                }
            });
    }

    ngOnInit() {
    }

    getAllData() {
        this.api.getJobs()
            .orderByChild('enterprise/key')
            .equalTo(this.enterprise.key)
            .on('value', Snapshots => {
                this.jobs = this.snapshotToArray(Snapshots);
                this.jobsData = true;
                this.forceUpdate();
            });
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

}
