import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IonSlides, LoadingController, AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
    selector: 'app-starter',
    templateUrl: './starter.page.html',
    styleUrls: ['./starter.page.scss'],
})
export class StarterPage implements OnInit {
    @ViewChild(IonSlides) slides: IonSlides;

    slideOpts = {
        effect: 'flip',
        simulateTouch: false,
        paginationType: 'progress'
    };

    user: any;
    branches: any = [];

    firstStepForm: FormGroup;
    secondStepForm: FormGroup;

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public api: ApiService,
        public formBuilder: FormBuilder,
        public alertCtrl: AlertController
    ) {
        this.firstStepForm = this.formBuilder.group({
            image: ['', [Validators.required]],
            email_contact: ['', [Validators.required]],
            phone_contact: ['', [Validators.required]]
        });

        this.secondStepForm = this.formBuilder.group({
            branches: ['', [Validators.required]],
        });

        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getEnterpriseUser(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        this.user.key = Snapshot.key;
                        if (this.user) {
                            loader.dismiss();
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',

        });
    }


    prevSlide() {
        this.slides.lockSwipes(false);
        this.slides.slidePrev();
        this.slides.lockSwipes(true);
    }

    nextSlide() {
        this.slides.lockSwipes(false);
        this.slides.slideNext();
        this.slides.lockSwipes(true);
    }

    updateProfile() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.starterInfoEnterprise(this.user.enterprise.key, this.firstStepForm.value, this.secondStepForm.value).then(data => {
                    loader.dismiss();
                    this.nextSlide();
                });
            })
        });
    }

    finish() {
        this.router.navigateByUrl('/dashboard/empresas', { replaceUrl: true });
    }

    async addBranch() {
        const alert = await this.alertCtrl.create({
            header: 'Agrega una sucursal',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre de sucursal'
                },
                {
                    name: 'address',
                    type: 'text',
                    placeholder: 'Dirección'
                },
                {
                    name: 'number',
                    type: 'number',
                    placeholder: 'Numero telefonico'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: data => {
                        this.branches.push(data);
                        this.secondStepForm.controls['branches'].setValue(this.branches);
                    }
                }
            ]
        });

        await alert.present();
    }

    RemoveBranch(branch, index) {
        this.branches.splice(index, 1);
        if (this.isEmpty(this.branches)) {
            this.secondStepForm.controls['branches'].setValue('');
        } else {
            this.secondStepForm.controls['branches'].setValue(this.branches);
        }
    }

    isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    onUploadFinished(file) {
        this.firstStepForm.controls['image'].setValue(file.src);
    }

}
