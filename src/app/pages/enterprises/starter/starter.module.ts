import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StarterPage } from './starter.page';
import { ImageUploadModule } from 'angular2-image-upload';

const routes: Routes = [
  {
    path: '',
    component: StarterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ImageUploadModule.forRoot(),
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StarterPage]
})
export class StarterPageModule {}
