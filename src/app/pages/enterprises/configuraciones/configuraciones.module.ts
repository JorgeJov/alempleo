import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConfiguracionesPage } from './configuraciones.page';
import { SharedEnterpriseComponentsModule } from 'src/app/components/shared-enterprise-components.module';
import { ImageUploadModule } from 'angular2-image-upload';

const routes: Routes = [
  {
    path: '',
    component: ConfiguracionesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ImageUploadModule.forRoot(),
    RouterModule.forChild(routes),
    SharedEnterpriseComponentsModule
  ],
  declarations: [ConfiguracionesPage]
})
export class ConfiguracionesPageModule {}
