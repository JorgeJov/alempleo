import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { PopoverController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.page.html',
    styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

    user: any;
    enterprise: any;

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverCtrl: PopoverController,
        public api: ApiService,
        private zone: NgZone
    ) {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getEnterpriseUser(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getEnterprise(this.user.enterprise.key)
                                .on('value', Snapshot => {
                                    this.enterprise = Snapshot.val();
                                    this.enterprise.key = Snapshot.key;
                                    this.api.getPicture(this.enterprise.key)
                                        .on('value', Snapshot => {
                                            this.enterprise.image = Snapshot.val();
                                            this.init();
                                            loader.dismiss();
                                        });
                                });
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    notifications: any;
    number_notifications: any;
    notifications_olds: any;

    init() {
        this.api.getNotifications(this.enterprise.key)
            .orderByChild('new')
            .equalTo(true)
            .on('value', snapshots => {
                this.notifications = [];
                this.enterprise.notifications = snapshots.numChildren();
                snapshots.forEach(element => {
                    let notification = element.val();
                    notification.key = element.key;
                    this.notifications.push(notification);
                });
                console.log(this.notifications);
                
                this.forceUpdate();
            });
            


        // this.api.getNotifications()
        //     .orderByChild('new')
        //     .equalTo(false)
        //     .limitToLast(10)
        //     .on('value', snapshots => {
        //         this.notifications_olds = [];
        //         snapshots.forEach(element => {
        //             let notification = element.val();
        //             notification.key = element.key;
        //             this.notifications.push(notification);
        //         });
        //         this.forceUpdate();
        //     });
    };

    ngOnInit() {
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    markAsRead(notification) {
        this.api.markReadANotification(notification).then(data=>{
            console.log('key');
            
        })
    }

    markAllAsRead() {
        this.notifications.forEach(element => {
            this.api.markReadANotification(element);
        });
    }

}
