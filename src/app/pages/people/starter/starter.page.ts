import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IonSlides, LoadingController, AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
    selector: 'app-starter',
    templateUrl: './starter.page.html',
    styleUrls: ['./starter.page.scss'],
})
export class StarterPage implements OnInit {
    // @ViewChild(IonSlides) slides: IonSlides;

    slideOpts = {
        effect: 'flip',
        simulateTouch: false,
        paginationType: 'progress'
    };
    user: any;
    work_items: any;

    experiences: any = [];

    starterForm: FormGroup;
    CVFile: any = null;

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public api: ApiService,
        public alertController: AlertController,
        public formBuilder: FormBuilder,
    ) {

        this.starterForm = this.formBuilder.group({
            image: [''],
            name: ['', [Validators.required]],
            lastname: ['', [Validators.required]],
            birthdate: ['', [Validators.required]],
            civil_status: [''],
            gender: ['', [Validators.required]],
            email: ['', [Validators.required]],
            number_1: ['', Validators.required],
            number_2: ['', Validators.required],
            number_3: [''],
            cv: [''],
            address: [''],
            dui: [''],
            nit: [''],
            nup: [''],
            experience: [''],
            studys: [''],
            careers: [''],
            courses: [''],
            references: [''],
            interests: [''],
        });
        console.log(this.auth.token);

        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getApplicant(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {

                            this.starterForm = this.formBuilder.group({
                                image: [''],
                                name: [this.user.name, [Validators.required]],
                                lastname: [this.user.lastname, [Validators.required]],
                                birthdate: [this.user.birthdate, [Validators.required]],
                                civil_status: [''],
                                gender: [this.user.gender, [Validators.required]],
                                email: [this.user.email, [Validators.required]],
                                number_1: [this.user.number_1, [Validators.required]],
                                number_2: [this.user.number_2, [Validators.required]],
                                number_3: [this.user.number_3, [Validators.required]],
                                cv: [''],
                                address: [this.user.address],
                                dui: [this.user.dui],
                                nit: [this.user.nit],
                                nup: [this.user.nup],
                                experience: [this.user.experience],
                                studys: [this.user.studys],
                                careers: [this.user.careers],
                                courses: [this.user.courses],
                                references: [this.user.references],
                                interests: [this.user.interests],
                            });
                            if (this.user.experience) {
                                this.experiences = this.user.experience;
                            }
                            if (this.user.studys) {
                                this.studys = this.user.studys;
                            }
                            if (this.user.careers) {
                                this.careers = this.user.careers;
                            }
                            if (this.user.courses) {
                                this.courses = this.user.courses;
                            }
                            if (this.user.references) {
                                this.references = this.user.references;
                            }




                            this.api.getWorkItems()
                                .on('value', Snapshots => {
                                    this.work_items = [];
                                    Snapshots.forEach(element => {
                                        let work_item = element.val();
                                        work_item.key = element.key;
                                        work_item.added = false;
                                        if (this.user.interests) {
                                            if (this.containsObject(work_item, this.user.interests)) {
                                                work_item.added = true;
                                            }
                                        }
                                        this.work_items.push(work_item);
                                    });
                                    loader.dismiss();
                                });

                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {

    }

    containsObject(obj, list) {
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i].key == obj.key) {
                return true;
            }
        }

        return false;
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
        });
    }

    interests = [];

    AddInterest(interest) {
        this.interests.push(interest);
        interest.added = true;
        this.starterForm.controls['interests'].setValue(this.interests);
    }

    RemoveInterest(index) {
        this.interests.splice(index, 1);
        if (this.isEmpty(this.interests)) {
            this.starterForm.controls['interests'].setValue('');
        } else {
            this.starterForm.controls['interests'].setValue(this.interests);
        }
    }

    async addExperience() {
        const alert = await this.alertController.create({
            header: 'Agrega tu experiencia Laboral',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre de la empresa'
                },
                {
                    name: 'work_item',
                    type: 'text',
                    placeholder: 'Giro'
                },
                {
                    name: 'phone',
                    type: 'text',
                    placeholder: 'Telefono'
                },
                {
                    name: 'position',
                    type: 'text',
                    placeholder: 'Cargo desempeñado'
                },
                {
                    name: 'main_functions',
                    type: 'text',
                    placeholder: 'Principales funciones realizadas'
                },
                {
                    name: 'chief',
                    type: 'text',
                    placeholder: 'Nombre de Jefe inmediato'
                },
                {
                    name: 'chief_position',
                    type: 'text',
                    placeholder: 'Cargo de Jefe inmediato'
                },
                {
                    name: 'start',
                    type: 'date',
                    placeholder: 'Fecha de ingreso'
                },
                {
                    name: 'end',
                    type: 'date',
                    placeholder: 'Fecha de salida'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.experiences.push(data);
                        this.starterForm.controls['experience'].setValue(this.experiences);
                    }
                }
            ]
        });

        await alert.present();
    }

    RemoveExperience(experience, index) {
        this.experiences.splice(index, 1);
        if (this.isEmpty(this.experiences)) {
            this.starterForm.controls['experience'].setValue('');
        } else {
            this.starterForm.controls['experience'].setValue(this.experiences);
        }
    }

    careers = [];

    async addCareer() {
        const alert = await this.alertController.create({
            header: 'Agregar estudio superior',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Carrera'
                },
                {
                    name: 'institution',
                    type: 'text',
                    placeholder: 'Institución'
                },
                {
                    name: 'level',
                    type: 'text',
                    placeholder: 'Nivel alcanzado'
                },
                {
                    name: 'period',
                    type: 'text',
                    placeholder: 'Periodo'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.careers.push(data);
                        this.starterForm.controls['careers'].setValue(this.careers);
                    }
                }
            ]
        });
        await alert.present();
    }

    RemoveCareer(career, index) {
        this.careers.splice(index, 1);
        if (this.isEmpty(this.careers)) {
            this.starterForm.controls['careers'].setValue('');
        } else {
            this.starterForm.controls['careers'].setValue(this.careers);
        }
    }

    studys: any = [];

    async addStudy() {
        const alert = await this.alertController.create({
            header: 'Agregar nivel de estudio',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'institution',
                    type: 'text',
                    placeholder: 'Institución'
                },
                {
                    name: 'period',
                    type: 'text',
                    placeholder: 'Periodo'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.studys.push(data);
                        this.starterForm.controls['studys'].setValue(this.studys);
                    }
                }
            ]
        });
        await alert.present();
    }

    RemoveStudy(study, index) {
        this.studys.splice(index, 1);
        if (this.isEmpty(this.studys)) {
            this.starterForm.controls['studys'].setValue('');
        } else {
            this.starterForm.controls['studys'].setValue(this.studys);
        }
    }

    courses: any = [];

    async addCourse() {
        const alert = await this.alertController.create({
            header: 'Agregar curso o seminarios',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre'
                },
                {
                    name: 'institution',
                    type: 'text',
                    placeholder: 'Institución'
                },
                {
                    name: 'duration',
                    type: 'text',
                    placeholder: 'Duración'
                },
                {
                    name: 'date',
                    type: 'date',
                    placeholder: 'Fecha'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.courses.push(data);
                        this.starterForm.controls['courses'].setValue(this.courses);
                    }
                }
            ]
        });
        await alert.present();
    }

    RemoveCourse(course, index) {
        this.courses.splice(index, 1);
        if (this.isEmpty(this.courses)) {
            this.starterForm.controls['courses'].setValue('');
        } else {
            this.starterForm.controls['courses'].setValue(this.courses);
        }
    }

    references: any = [];

    async addReference() {
        const alert = await this.alertController.create({
            header: 'Agregar una referencia',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre'
                },
                {
                    name: 'enterprise',
                    type: 'text',
                    placeholder: 'Empresa'
                },
                {
                    name: 'position',
                    type: 'text',
                    placeholder: 'Cargo'
                },
                {
                    name: 'phone',
                    type: 'text',
                    placeholder: 'Telefono'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.references.push(data);
                        this.starterForm.controls['references'].setValue(this.references);
                    }
                }
            ]
        });
        await alert.present();
    }

    RemoveReference(course, index) {
        this.references.splice(index, 1);
        if (this.isEmpty(this.references)) {
            this.starterForm.controls['references'].setValue('');
        } else {
            this.starterForm.controls['references'].setValue(this.references);
        }
    }

    isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    updateProfile() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.starterInfo(this.starterForm.value, this.CVFile).then(data => {
                    loader.dismiss();
                });
            })
        })
    }

    upload(event) {
        this.CVFile = event.target.files[0]
        this.starterForm.controls['cv'].setValue('ya');
    }

    finish() {
        this.router.navigateByUrl('/dashboard/personas', { replaceUrl: true });
    }

    onUploadFinished(file) {
        this.starterForm.controls['image'].setValue(file.src);
    }
}
