import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import {ImageUploadModule} from "angular2-image-upload";
import { StarterPage } from './starter.page';
import { SharedPeopleComponentsModule } from 'src/app/components/shared-people-components.module';

const routes: Routes = [
  {
    path: '',
    component: StarterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedPeopleComponentsModule,
    ImageUploadModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [StarterPage]
})
export class StarterPageModule {}
