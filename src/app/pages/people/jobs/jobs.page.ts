import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-jobs',
    templateUrl: './jobs.page.html',
    styleUrls: ['./jobs.page.scss'],
})
export class JobsPage implements OnInit {

    jobsData: boolean = false;
    workItemsData: boolean = false;
    work_items: any = [];
    jobs: any = [];
    user: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        private auth: AuthService
    ) {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getApplicant(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        this.user.key = Snapshot.key;
                        if (this.user) {
                            this.api.getPicture(this.user.key)
                                .on('value', Snapshot => {
                                    this.user.image = Snapshot.val();
                                    loader.dismiss();
                                    this.getData();
                                })
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {

    }

    getData() {

        this.jobs = [];
        this.api.getJobs()
            .once('value', snapshots => {
                snapshots.forEach(snapshot => {
                    let item = snapshot.val();
                    if (item.status == 'abierta') {
                        item.key = snapshot.key;
                        this.api.getPicture(item.enterprise.key)
                            .on('value', Snapshot => {
                                item.enterprise.image = Snapshot.val();
                                this.jobs.push(item);
                                this.api.getNotifications(this.user.key)
                                    .orderByChild('new')
                                    .equalTo(true)
                                    .on('value', snapshots => {
                                        this.user.notifications = snapshots.numChildren();
                                        this.forceUpdate();
                                    });
                            })
                    }
                });
                this.jobsData = true;
                this.forceUpdate();
            });

        // this.api.getWorkItems()
        //     .on('value', Snapshots => {
        //         this.work_items = [];
        //         this.work_items = this.snapshotToArray(Snapshots);
        //         this.workItemsData = true;
        //         this.forceUpdate();
        //     })
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

}
