import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConfigurationsPage } from './configurations.page';
import { SharedPeopleComponentsModule } from 'src/app/components/shared-people-components.module';
import { ImageUploadModule } from 'angular2-image-upload';

const routes: Routes = [
  {
    path: '',
    component: ConfigurationsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    ImageUploadModule.forRoot(),
    SharedPeopleComponentsModule
  ],
  declarations: [ConfigurationsPage]
})
export class ConfigurationsPageModule {}
