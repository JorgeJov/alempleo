import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoginOptionsComponent } from 'src/app/components/login-options/login-options.component';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    loginForm: FormGroup;

    constructor(
        public formBuilder: FormBuilder,
        private auth: AuthService,
        public alertCtrl: AlertController,
        private router: Router,
        public loadingCtrl: LoadingController,
        public popoverController: PopoverController,
        public api: ApiService
    ) {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]]
        });
    }

    ngOnInit() {
        console.log('Login Page');
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    login() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.login(this.loginForm.value.email, this.loginForm.value.password).then(data => {
                    loader.dismiss();
                    this.router.navigate(['/dashboard/personas'])
                }, err => {
                    this.alertCtrl.create({
                        header: '¡Ups!',
                        // subHeader: 'Subtitle',
                        message: 'Hay un error con tus correo o contraseña, intentalo nuevamente',
                        buttons: ['Aceptar']
                    }).then(alert => {
                        loader.dismiss();
                        alert.present();
                    })
                });
            });
        });
    }

    async forgotPassword() {
        const alert = await this.alertCtrl.create({
            header: 'Ingresa tu dirección de correo electronico',
            inputs: [
                {
                    name: 'email',
                    type: 'text',
                    placeholder: 'Correo electronico'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        this.auth.resetPassword(data.email).then(data => {
                            this.alertCtrl.create({
                                header: 'Restaurar contraseña',
                                message: 'Hemos enviado un link a tu correo electornico para que puedas restaurar tu contraseña',
                                buttons: ['Aceptar']
                            }).then(alert => {
                                alert.present();
                            })
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    async presentPopover(ev: any) {
        const popover = await this.popoverController.create({
            component: LoginOptionsComponent,
            event: ev,
            translucent: true
        });
        return await popover.present();
    }

    // work_items = [
    //     {
    //         "name": "Técnicos en Limpieza"
    //     },
    //     {
    //         "name": "Mensajero"
    //     },
    //     {
    //         "name": "Secretaria / recepcionista"
    //     },
    //     {
    //         "name": "Asistente administrativa"
    //     },
    //     {
    //         "name": "Auxiliar contable"
    //     },
    //     {
    //         "name": "Contador"
    //     },
    //     {
    //         "name": "Recursos Humanos"
    //     },
    //     {
    //         "name": "Operario"
    //     },
    //     {
    //         "name": "Atención al cliente y Ventas"
    //     },
    //     {
    //         "name": "Jardineros"
    //     },
    //     {
    //         "name": "Meseros"
    //     },
    //     {
    //         "name": "Electricistas"
    //     },
    //     {
    //         "name": "Aux. de cocina y cocineros"
    //     },
    //     {
    //         "name": "Motoristas"
    //     },
    //     {
    //         "name": "Display"
    //     },
    //     {
    //         "name": "Diseñador Grafico"
    //     },
    //     {
    //         "name": "Área de belleza (Esteticistas y cosmetologas)"
    //     },
    //     {
    //         "name": "Profesionales de servicio a domicilio"
    //     },
    //     {
    //         "name": "Bodega"
    //     },
    //     {
    //         "name": "Cajeros"
    //     },
    //     {
    //         "name": "Masoterapia y terapia de la piel"
    //     },
    //     {
    //         "name": "Teleoperadores"
    //     },
    //     {
    //         "name": "Seguridad"
    //     },
    //     {
    //         "name": "Cobradores"
    //     },
    //     {
    //         "name": "DJ/Animador"
    //     },
    //     {
    //         "name": "Enfermeria"
    //     },
    //     {
    //         "name": "Asistente Dental"
    //     },
    //     {
    //         "name": "Docente"
    //     },
    //     {
    //         "name": "Encuetador"
    //     },
    //     {
    //         "name": "encargado de supermercado"
    //     },
    //     {
    //         "name": "Tecnico Mantenimiento de Computadoras"
    //     },
    //     {
    //         "name": "Encargado/Auxiliar de Archivo"
    //     },
    //     {
    //         "name": "Estructuras Metalicas"
    //     },
    //     {
    //         "name": "Costurera"
    //     },
    //     {
    //         "name": "oficios Varios"
    //     },
    //     {
    //         "name": "Gestor de Cobros"
    //     },
    //     {
    //         "name": "Mecanica Automotriz"
    //     },
    //     {
    //         "name": "Niñera"
    //     },
    //     {
    //         "name": "Consultor Cuidadano/Censo"
    //     },
    //     {
    //         "name": "Promotor de Alfabetizacion"
    //     },
    //     {
    //         "name": "Digitador"
    //     },
    //     {
    //         "name": "Pintor"
    //     },
    //     {
    //         "name": "Albañil"
    //     },
    //     {
    //         "name": "Enderezado y Pintura"
    //     },
    //     {
    //         "name": "Tecnico Biomedico"
    //     },
    //     {
    //         "name": "Supervisor de Ventas"
    //     },
    //     {
    //         "name": "Fotografo"
    //     },
    //     {
    //         "name": "Panadero"
    //     },
    //     {
    //         "name": "Electronica"
    //     },
    //     {
    //         "name": "Psicologo"
    //     },
    //     {
    //         "name": "Manicurista y/o pedicurista"
    //     },
    //     {
    //         "name": "Impulsadora"
    //     },
    //     {
    //         "name": "operador"
    //     },
    //     {
    //         "name": "carpintero"
    //     },
    //     {
    //         "name": "trabajador social"
    //     },
    //     {
    //         "name": "Dependiente de farmacia"
    //     },
    //     {
    //         "name": "bartender"
    //     },
    //     {
    //         "name": "turismo"
    //     }
    // ]

    // addWorkItems() {
    //     this.work_items.forEach(element => {
    //         this.api.addWorkItem(element);
    //     });
    // }
}

