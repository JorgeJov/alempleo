import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-job',
    templateUrl: './job.page.html',
    styleUrls: ['./job.page.scss'],
})
export class JobPage implements OnInit {

    user: any;
    jobs: any = [];
    job_key: any = [];
    jobsData: boolean = false;
    jobData: boolean = false;
    already_applied: boolean = false;
    job: any;

    constructor(
        private route: ActivatedRoute,
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public popoverCtrl: PopoverController,
        public api: ApiService,
        public zone: NgZone,
        public alertController: AlertController
    ) {
        this.job_key = this.route.snapshot.paramMap.get('key');

        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getApplicant(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getPicture(this.user.key)
                                .on('value', Snapshot => {
                                    this.user.image = Snapshot.val();
                                    this.api.getNotifications(this.user.key)
                                        .orderByChild('new')
                                        .equalTo(true)
                                        .on('value', snapshots => {
                                            this.user.notifications = snapshots.numChildren();
                                            loader.dismiss();
                                            this.getData();
                                            this.forceUpdate();
                                        });
                                })
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {
    }

    getData() {
        this.api.GetJobApplicant(this.job_key)
            .orderByChild('applicant_key')
            .equalTo(this.auth.token)
            .on('value', Snapshot => {
                if (Snapshot.val()) {
                    this.already_applied = true;
                } else {
                    this.already_applied = false;
                }
            });

        this.api.getJob(this.job_key)
            .on('value', Snapshot => {
                this.jobData = true;
                this.job = Snapshot.val();
                this.job.key = Snapshot.key;
                this.api.getPicture(this.job.enterprise.key)
                    .on('value', Snapshot => {
                        this.job.enterprise.image = Snapshot.val();
                        this.forceUpdate();
                    })
            });

        // this.api.getJobs()
        //     .limitToLast(10)
        //     .on('value', snapshots => {
        //         this.jobs = this.snapshotToArray(snapshots);
        //         this.jobsData = true;
        //         this.forceUpdate();
        //     });
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    async applyToAJob() {
        if (this.user.status == 'active') {
            const alert = await this.alertController.create({
                header: ``,
                message: `Solo confirma que deseas aplicar a la plaza <strong>${this.job.position}</strong> de <strong>${this.job.enterprise.name}</strong>?`,
                cssClass: 'applyToJob',
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'cancelButtonAlert',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Aplicar',
                        cssClass: 'confirmButtonAlert',
                        handler: () => {
                            this.api.applyToAJob(this.job, this.user).then(data => {

                            });
                        }
                    }
                ]
            });

            await alert.present();
        } else {
            const alert = await this.alertController.create({
                header: 'Cuenta no activada',
                message: 'Debes tener activada tu cuenta para poder aplicar a una plaza, en un momento el personal de Alempleo activara tu cuenta.',
                buttons: ['OK']
            });

            await alert.present();
        }



    }

}
