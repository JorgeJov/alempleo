import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoginOptionsComponent } from 'src/app/components/login-options/login-options.component';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.page.html',
    styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

    singUpForm: FormGroup;
    isLoading = false;

    constructor(
        public formBuilder: FormBuilder,
        public auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverController: PopoverController
    ) {
        this.singUpForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            lastname: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });

        this.loader().then(loader => {
            loader.present().then(() => {

                loader.dismiss();
            });
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    ngOnInit() {
    }

    signUp() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.signUpApplicant(this.singUpForm.value).then(data => {
                    this.router.navigate(['/dashboard/personas/'])
                    loader.dismiss();
                }, err => {
                    loader.dismiss();
                });
            });
        });
    }

    async presentPopover(ev: any) {
        const popover = await this.popoverController.create({
            component: LoginOptionsComponent,
            event: ev,
            translucent: true
        });
        return await popover.present();
    }

}
