import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DashboardPage } from './dashboard.page';
import { SharedPeopleComponentsModule } from 'src/app/components/shared-people-components.module';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  }
]; 

@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedPeopleComponentsModule
  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
