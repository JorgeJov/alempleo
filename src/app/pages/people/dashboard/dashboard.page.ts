import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { ProfilePopoverComponent } from 'src/app/components/profile-popover/profile-popover.component';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})

export class DashboardPage implements OnInit {

    user: any;
    jobs: any = [];
    jobsData: boolean = false;
    applications: any = [];

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverCtrl: PopoverController,
        public api: ApiService,
        public zone: NgZone,

    ) {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getApplicant(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getPicture(this.user.key)
                                .on('value', Snapshot => {
                                    this.user.image = Snapshot.val();
                                    this.api.getNotifications(this.user.key)
                                        .orderByChild('new')
                                        .equalTo(true)
                                        .on('value', snapshots => {
                                            this.user.notifications = snapshots.numChildren();
                                            loader.dismiss();
                                            if(this.user.status == 'starter'){
                                                this.router.navigate(['/cuestionario/personas'])
                                                this.forceUpdate();
                                            }else{
                                                this.forceUpdate();
                                                this.getData();
                                            }
                                        });
                                })
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    getData() {
        for (var key in this.user.work_items) {
            var obj = this.user.work_items[key];
            this.api.getJobs()
                .orderByChild('kind_job/key')
                .equalTo(key)
                .limitToLast(3)
                .once('value', snapshots => {
                    snapshots.forEach(snapshot => {
                        let item = snapshot.val();
                        if (item.status == 'abierta') {
                            item.key = snapshot.key;
                            this.api.getPicture(item.enterprise.key)
                                .on('value', Snapshot => {
                                    item.enterprise.image = Snapshot.val();
                                    this.jobs.push(item);
                                    this.api.getNotifications(this.user.key)
                                        .orderByChild('new')
                                        .equalTo(true)
                                        .on('value', snapshots => {
                                            this.user.notifications = snapshots.numChildren();
                                            this.forceUpdate();
                                        });
                                })
                        }
                    });
                    this.jobsData = true;
                    this.forceUpdate();
                });
        }


        this.api.GetApplicantions()
            .orderByChild('applicant/key')
            .equalTo(this.user.key)
            .on('value', snapshots => {
                this.applications = [];
                snapshots.forEach(element => {
                    let application = element.val();
                    application.key = element.key;

                    this.api.getJob(application.job.key)
                        .on('value', snapshot => {
                            let job = snapshot.val();
                            job.key = snapshot.key;
                            if (job.status == 'abierta') {
                                this.api.getPicture(job.enterprise.key)
                                    .on('value', Snapshot => {
                                        job.enterprise.image = Snapshot.val();
                                        application.job = job;
                                        this.applications.push(application);
                                    })
                            }

                        })
                });
            });
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    ngOnInit() {
    }

    async presentProfilePopover(ev: any) {
        const popover = await this.popoverCtrl.create({
            component: ProfilePopoverComponent,
            event: ev,
        });
        return await popover.present();
    }


}
