import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ApiService } from 'src/app/services/api/api.service';
import { AlertController, LoadingController, PopoverController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

    user: any;
    ConfigurationsForm: FormGroup;

    constructor(
        private auth: AuthService,
        public api: ApiService,
        public alertCtrl: AlertController,
        public formBuilder: FormBuilder,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverCtrl: PopoverController,
    ) {

        this.ConfigurationsForm = this.formBuilder.group({
            email: ['', [Validators.required]],
        });

        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getApplicant(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getPicture(this.user.key)
                                .on('value', Snapshot => {
                                    this.user.image = Snapshot.val();
                                    this.api.getNotifications(this.user.key)
                                        .orderByChild('new')
                                        .equalTo(true)
                                        .on('value', snapshots => {
                                            this.user.notifications = snapshots.numChildren();
                                            loader.dismiss();
                                        });
                                })
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

}
