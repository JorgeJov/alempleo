import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingController, PopoverController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.page.html',
    styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

    user: any;
    search_query: any;
    jobs: any;
    searching: any = false;
    searchTerm: string = '';
    filter: any;

    constructor(
        private auth: AuthService,
        private route: ActivatedRoute,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverCtrl: PopoverController,
        public api: ApiService,
        public zone: NgZone,
    ) {
        let query = this.route.snapshot.paramMap.get('query');
        this.searchTerm = query.replace("_", " ")

        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getApplicant(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getPicture(this.user.key)
                                .on('value', Snapshot => {
                                    this.user.image = Snapshot.val();
                                    this.api.getNotifications(this.user.key)
                                        .orderByChild('new')
                                        .equalTo(true)
                                        .on('value', snapshots => {
                                            this.user.notifications = snapshots.numChildren();
                                            loader.dismiss();
                                            this.forceUpdate();
                                            this.init();
                                        });
                                })
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    ngOnInit() {
    }

    init() {
        this.searchTerm = this.route.snapshot.paramMap.get('query');

        this.api.getJobs()
            .orderByChild('status')
            .equalTo('abierta')
            .on('value', Snapshots => {
                this.jobs = [];
                Snapshots.forEach(element => {
                    let job = element.val();
                    job.key = element.key;
                    job.searchQ = `${job.type} ${job.description} ${job.position} ${job.gender} ${job.experience} ${job.branch.address} ${job.branch.name} ${job.branch.number} ${job.enterprise.name} ${job.enterprise.email} ${job.enterprise.phone} ${job.kind_job.name}`;
                    this.jobs.push(job);
                });
                this.filterSearch();
            })
    }

    filterSearch() {
        let searchTerm = this.searchTerm;
        if (searchTerm == '') {
            this.filter = this.jobs;
        } else {
            this.filter = this.jobs.filter((item) => {
                return item.searchQ.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
        }
        console.log(this.filter);
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

}