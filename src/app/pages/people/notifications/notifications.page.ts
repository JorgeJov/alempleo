import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.page.html',
    styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

    user: any;

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        public router: Router,
        public popoverCtrl: PopoverController,
        public api: ApiService,
        public zone: NgZone,
    ) {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.getApplicant(this.auth.token)
                    .on('value', Snapshot => {
                        this.user = Snapshot.val();
                        if (this.user) {
                            this.user.key = Snapshot.key;
                            this.api.getPicture(this.user.key)
                                .on('value', Snapshot => {
                                    this.user.image = Snapshot.val();
                                    loader.dismiss();
                                    this.init();
                                    this.forceUpdate();
                                })
                        } else {
                            loader.dismiss();
                            this.auth.logOut();
                        }
                    });
            });
        });
    }

    notifications: any;

    init() {
        this.api.getNotifications(this.user.key)
            .orderByChild('new')
            .equalTo(true)
            .on('value', snapshots => {
                this.notifications = [];
                this.user.notifications = snapshots.numChildren();
                snapshots.forEach(element => {
                    let notification = element.val();
                    notification.key = element.key;
                    this.notifications.push(notification);
                });
                this.forceUpdate();
            });
    }

    ngOnInit() { }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    markAsRead(notification) {
        this.api.markReadANotificationPerson(notification);
    }

    markAllAsRead() {
        this.notifications.forEach(element => {
            this.api.markReadANotificationPerson(element);
        });
    }

}
