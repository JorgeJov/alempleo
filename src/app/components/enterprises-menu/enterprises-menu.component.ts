import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { AlertController } from '@ionic/angular';

@Component({
    selector: 'app-enterprises-menu',
    templateUrl: './enterprises-menu.component.html',
    styleUrls: ['./enterprises-menu.component.scss'],
})
export class EnterprisesMenuComponent implements OnInit {


    @Input('user') user: any;
    @Input('enterprise') enterprise: any;

    constructor(
        public alertController: AlertController,
        public api: ApiService
    ) {

    }


    ngOnInit() { }

    async cantAdd() {
        const alert = await this.alertController.create({
          header: 'Cuenta no activada',
          message: 'Debes tener activada tu cuenta para poder agregar una plaza, en un momento el personal de Alempleo activara tu cuenta.',
          buttons: ['OK']
        });
    
        await alert.present();
      }

}
