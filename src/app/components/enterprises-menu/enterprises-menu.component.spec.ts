import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterprisesMenuPage } from './enterprises-menu.page';

describe('EnterprisesMenuPage', () => {
  let component: EnterprisesMenuPage;
  let fixture: ComponentFixture<EnterprisesMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterprisesMenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterprisesMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
