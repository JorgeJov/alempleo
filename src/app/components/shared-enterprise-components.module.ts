import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { EnterprisesMenuComponent } from './enterprises-menu/enterprises-menu.component';
import { EnterprisesHeaderComponent } from './enterprises-header/enterprises-header.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        IonicModule
    ],
    declarations: [
        EnterprisesMenuComponent,
        EnterprisesHeaderComponent
    ],
    exports: [
        EnterprisesMenuComponent,
        EnterprisesHeaderComponent
    ]
})
export class SharedEnterpriseComponentsModule { }
