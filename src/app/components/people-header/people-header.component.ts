import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfilePopoverComponent } from '../profile-popover/profile-popover.component';
import { PopoverController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-people-header',
    templateUrl: './people-header.component.html',
    styleUrls: ['./people-header.component.scss'],
})
export class PeopleHeaderComponent implements OnInit {

    @Input('user') user: any;

    constructor(
        public popoverCtrl: PopoverController,
        public api: ApiService,
        public router: Router,
        private route: ActivatedRoute
    ) {
        
    }

    
    ngOnInit() {

    }

    ngOnChanges() {
    }

    async presentProfilePopover(ev: any) {
        const popover = await this.popoverCtrl.create({
            component: ProfilePopoverComponent,
            event: ev,
        });
        return await popover.present();
    }

    search(ev){
        // let query = ev.target.value.replace(" ", "_")
        this.router.navigate([`/buscar/${ev.target.value}/`])
    }

}
