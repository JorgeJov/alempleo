import { Component, OnInit, NgZone } from '@angular/core';
import { NavParams, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-applicant',
    templateUrl: './applicant.component.html',
    styleUrls: ['./applicant.component.scss'],
})
export class ApplicantComponent implements OnInit {

    applicant: any;
    job: any;
    contactData: boolean = false;
    interests: any = [];
    constructor(
        navParams: NavParams,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
    ) {
        this.applicant = navParams.get('applicant');
        this.job = navParams.get('job');

        if (this.applicant.birthdate) {
            this.applicant.birthdate = this.calculateAge(new Date(this.applicant.birthdate));
        }
        for (var prop in this.applicant.interests) {
            this.interests.push(this.applicant.interests[prop]);
        }
    }

    calculateAge(birthday) {
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    ngOnInit() {

    }

    changeContactData(){
        if(this.contactData){
            this.contactData = false;
        }else{
            this.contactData = true;
        }
    }

    applyToAJob(){
        
    }

    downloadCV(){
        this.api.GetApplicant(this.applicant.applicant_key)
        .once('value', Snapshot=>{
            // console.log(Snapshot.val());
            window.open(Snapshot.val().cv, "_blank")  
        })
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    

}
