import { Component, OnInit, Input } from '@angular/core';
import { ProfilePopoverComponent } from '../profile-popover/profile-popover.component';
import { PopoverController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-enterprises-header',
    templateUrl: './enterprises-header.component.html',
    styleUrls: ['./enterprises-header.component.scss'],
})
export class EnterprisesHeaderComponent implements OnInit {

    @Input('user') user: any;
    @Input('enterprise') enterprise: any;

    constructor(
        public popoverCtrl: PopoverController,
        public api: ApiService
    ) {
        
    }

    ngOnInit() {

    }

    async presentProfilePopover(ev: any) {
        const popover = await this.popoverCtrl.create({
            component: ProfilePopoverComponent,
            event: ev,
        });
        return await popover.present();
    }

}
