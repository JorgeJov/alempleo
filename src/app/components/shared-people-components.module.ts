import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeopleMenuComponent } from './people-menu/people-menu.component';
import { IonicModule } from '@ionic/angular';
import {  RouterModule } from '@angular/router';
import { PeopleHeaderComponent } from './people-header/people-header.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        IonicModule 
    ],
    declarations: [
        PeopleMenuComponent,
        PeopleHeaderComponent
    ],
    exports: [
        PeopleMenuComponent,
        PeopleHeaderComponent
    ]
})
export class SharedPeopleComponentsModule { }
